import { Component, OnInit } from '@angular/core';
import { PostService } from '../post.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-joke-display',
  templateUrl: './joke-display.component.html',
  styleUrls: ['./joke-display.component.css']
})
export class JokeDisplayComponent implements OnInit {

  constructor(private postService: PostService, private http: HttpClient) { }

  ngOnInit() {
    this.createAJoke();
  }
  title;
  setup;
  brokenJoke;
  properJoke;
  message;

  createAJoke() {
    this.http.get<any>('https://official-joke-api.appspot.com/random_joke').subscribe(joke => {
      console.log(joke);
      this.setup = joke.setup;
      this.brokenJoke = this.shuffle(joke.punchline);
      this.properJoke = joke.punchline;
    })
  }

  shuffle(punchline) {
    var a = punchline.split(""),
      n = a.length;

    for (var i = n - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var tmp = a[i];
      a[i] = a[j];
      a[j] = tmp;
    }
    return a.join("");
  }

  submitEntry(entryValue) {
    this.postService.submitEntry(entryValue, this.brokenJoke, this.properJoke).subscribe(msg => {
      this.message = msg;
      console.log(msg);
    });
  }
}
