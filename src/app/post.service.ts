import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpEvent,
  HttpEventType,
  HttpErrorResponse
} from "@angular/common/http";
import { catchError, last, map, tap } from "rxjs/operators";
import { of } from "rxjs";
import { MessageService } from "./message.service";
@Injectable({
  providedIn: "root"
})
export class PostService {
  constructor(private http: HttpClient, private messenger: MessageService) { }
  taskID: string = null;
  glossary: JSON = null;
  serverAddress = "http://localhost:3690/";
  submitEntry(jokeEntry: string, brokenJoke, properJoke) {
    if (!jokeEntry) {
      console.error("No joke")
    }
    return this.http
      .post(
        this.serverAddress + "joke/entry",
        {
          Entry: jokeEntry,
          Player: '3',
          JokeID: '6',
          Votes: 0,
          Score: 0,
          Comments: 'Very funny'
        },
        {
          reportProgress: true,
          observe: "response"
        }
      )
      .pipe(
        map(event => this.getEventMessage(event, jokeEntry)),
        tap(message => this.showProgress(message)),
        last(), // return last (completed) message to caller
        catchError(this.handleError(jokeEntry))
      );
  }

  private getEventMessage(event: HttpEvent<any>, moaCommands: string) {
    switch (event.type) {
      case HttpEventType.Sent:
        return `Posted "${moaCommands}".`;

      case HttpEventType.UploadProgress:
        // Compute and show the % done:
        const percentDone = Math.round((100 * event.loaded) / event.total);
        return `command "${moaCommands}" is ${percentDone}% posting.`;

      case HttpEventType.Response:
        this.taskID = event.body.taskID;
        this.glossary = event.body.glossary;
        return `Article input "${moaCommands}" sent! `;

      default:
        return `command "${moaCommands}" surprising upload event: ${event.type}.`;
    }
  }
  private showProgress(message: string) {
    this.messenger.add(message);
  }

  private handleError(command: string) {
    const userMessage = `${command} post failed.`;

    return (error: HttpErrorResponse) => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      const message =
        error.error instanceof Error
          ? error.error.message
          : `server returned code ${error.status} with body "${error.error}"`;

      this.messenger.add(`${userMessage} ${message}`);

      // Let app keep running but indicate failure.
      return of(userMessage);
    };
  }
}
